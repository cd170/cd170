#include<stdio.h>
#include<math.h>
 int input_1()
 {
int a;
printf("enter first number:  ");
scanf("%d",&a);
return a;
}

int input_2()
{
int b;
printf("enter second number:  ");
scanf("%d",&b);
return b;
}

int input_3()
{
int c;
printf("enter third number:  ");
scanf("%d",&c);
return c;
}

void roots(int a,int b,int c)
{
double discriminant, root1, root2, realPart, imagPart;
discriminant = b * b - 4 * a * c;

    if (discriminant > 0) {
        root1 = (-b + sqrt(discriminant)) / (2 * a);
        root2 = (-b - sqrt(discriminant)) / (2 * a);
        printf("root1 = %.2lf and root2 = %.2lf", root1, root2);
    }

    else if (discriminant == 0) {
        root1 = root2 = -b / (2 * a);
        printf("root1 = root2 = %.2lf;", root1);
    }

    else {
        realPart = -b / (2 * a);
        imagPart = sqrt(-discriminant) / (2 * a);
        printf("root1 = %.2lf+%.2lfi and root2 = %.2f-%.2fi", realPart, imagPart, realPart, imagPart);
    }

}
int main()
{
int p,q,r;
p=input_1();
q=input_2();
r=input_3();
roots(p,q,r);
return 0;
}

